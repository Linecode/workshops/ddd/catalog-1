---
name: SubscriptionCreated
version: 0.0.1
summary: |
  Event represents when an subscripton where created
producers:
    - Subscriptions Service
consumers:
	- Library Service
owners:
    - dboyne
    - mSmith
---

<Admonition>When firing this event make sure you set the `correlation-id` in the headers. Our schemas have standard metadata make sure you read and follow it.</Admonition>

### Details

This event is the final event of the ordering process. It gets raised when the shipment has been delivered.

<NodeGraph title="Consumer / Producer Diagram" />

<Schema />