---
name: Licenses Service
summary: |
  API to manage company licenses
owners:
    - dboyne
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut risus vitae enim imperdiet eleifend non eu sapien. Fusce vitae ligula pulvinar nibh consequat ullamcorper. Maecenas volutpat, mi in interdum porta, urna nibh tincidunt dolor, a malesuada eros sem tempus leo. Ut convallis libero et tempor convallis. Nam varius semper ante at dignissim. Pellentesque turpis diam, aliquet at dignissim quis, tristique vel dolor. Cras accumsan, eros sed gravida ultrices, lorem orci rutrum ex, ac scelerisque nulla libero fringilla ligula. Maecenas euismod eleifend mi ac aliquam. Vestibulum neque augue, sollicitudin sed lectus et, consequat sollicitudin nisi. Mauris vel iaculis tortor. Duis felis enim, convallis et varius non, dapibus sed mi. Cras vel arcu quis justo congue mattis.

<NodeGraph />